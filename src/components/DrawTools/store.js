import { writable } from 'svelte/store';

export const selectedTrace = writable(0);
export const traces = writable([]);